# Decoupled Blocks: React

This is a [React](https://react.dev) implementation for the
[Decoupled Blocks](https://www.drupal.org/project/pdb) module. Blocks built
with React can now encapsulate all that is needed for it and be added
to a site via a module or in a custom theme.

## React Version
This module is using the version 15 of React.
