/**
 * @file
 */

ReactDOM.render(React.createElement(
  'h1',
  {className: '.react-test'},
  'Hello, world!'
), document.getElementById('react-example-1'));
