<?php

namespace Drupal\Tests\pdb_react\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;

/**
 * React Render tests.
 *
 * @group pdb_react
 */
class ReactRenderTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'block',
    'pdb',
    'pdb_react',
  ];

  /**
   * Default Theme.
   */
  protected $defaultTheme = 'stark';

  /**
   * Test that a React instance renders.
   */
  public function testExample1Renders() {
    $assert = $this->assertSession();

    // Place the "React Example 1" block.
    $this->drupalPlaceBlock('react_component:react_example_1');

    $this->drupalGet('<front>');

    $assert->waitForElement('css', '.react-test');
    $assert->pageTextContains("Hello, world!");
  }

}
